package com.zzcx.springboot.递归;

import com.zzcx.springboot.utils.JsonUtils;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class RecursiveTest {
    @Test
    public void testRecursive() {
        List<Menu> menus = Arrays.asList(
                new Menu(1, "湖南省", 0, 0),
                new Menu(2, "长沙市", 1, 1),
                new Menu(3, "湘潭市", 1, 1),
                new Menu(4, "益阳市", 1, 1),
                new Menu(5, "雨花区", 2, 2),
                new Menu(6, "岳麓区", 2, 2),
                new Menu(7, "天心区", 2, 2),
                new Menu(8, "岳塘区", 3, 2),
                new Menu(9, "雨湖区", 3, 2),
                new Menu(10, "赫山区", 4, 2),
                new Menu(11, "资阳区", 4, 2)
        );

        //获取根节点
        /*List<Menu> list = menus.stream().filter(m -> m.getParentId() == 0).collect(Collectors.toList());
        getChildrens(list, menus);
        System.out.println(JsonUtils.objectToJson(list));*/

        final Map<Integer, List<Menu>> menuChildrenMap = new HashMap<>();
        List<Menu> collect = menus.stream().filter(menu -> {
            Integer id = menu.getId();
            Integer parentId = menu.getParentId();
            if (menuChildrenMap.get(id) == null) {
                menuChildrenMap.put(id, new LinkedList<>());
            }
            menu.setChildList(menuChildrenMap.get(id));
            if (parentId == 0) {
                return true;
            } else {
                if (menuChildrenMap.get(parentId) == null) {
                    menuChildrenMap.put(parentId, new LinkedList<>());
                }
                menuChildrenMap.get(parentId).add(menu);
            }
            return false;
        }).collect(Collectors.toList());
        System.out.println(JsonUtils.objectToJson(collect));
    }

    private void getChildrens(List<Menu> childrens, List<Menu> menus){
        if (childrens != null && childrens.get(0).getLevel() < 1){
            childrens.forEach(children -> {
                ArrayList<Menu> list = new ArrayList<>();
                menus.forEach(m -> {
                    if (children.getId() == m.getParentId()){
                        list.add(m);
                        children.setChildList(list);
                    }
                });
                getChildrens(children.getChildList(), menus);
            });
        }
    }
}
