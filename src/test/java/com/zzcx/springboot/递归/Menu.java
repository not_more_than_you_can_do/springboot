package com.zzcx.springboot.递归;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private Integer id;

    private String name;

    private Integer parentId;

    private Integer level;

    private List<Menu> childList = new ArrayList<>();

    public Menu(Integer id, String name, Integer parentId, Integer level) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.level = level;
    }

    public Menu(Integer id, String name, Integer parentId, Integer level, List<Menu> childList) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.level = level;
        this.childList = childList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public List<Menu> getChildList() {
        return childList;
    }

    public void setChildList(List<Menu> childList) {
        this.childList = childList;
    }
}
